#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <iostream>

/*
02 -  Escrever um algoritmo que l� a hora de in�cio e hora de t�rmino de um jogo, ambas subdivididas em dois valores distintos: 
horas e minutos. Calcular e escrever a dura��o do jogo, tamb�m em horas e minutos, considerando que o tempo m�ximo de dura��o de 
um jogo � de 24 horas e que o jogo pode iniciar em um dia e terminar no dia seguinte.
*/

int horaInicio, horaFinal, minutoInicio, minutoFinal;

char op;

main(){
	do{
		system("cls");
	INICIO:
	PEDIDOHORAINICIO:
	printf(" Insira \n HORARIO INICIAL(HH): ");
	scanf("%d",& horaInicio);

	if(horaInicio < 0 || horaInicio > 23) 
	{
		printf("\n Insira uma hora entre 0 e 23. \n\n");
		system("pause");
		system("cls");
		goto PEDIDOHORAINICIO; // Caso o numero inserido para o horario inicial for menor que 0 ou maior que 23 ele retorna, pedindo um novo valor
	}
	
	PEDIDOMINUTOINICIO:
	printf("\n MINUTOS INICIAIS(MM): ");
	scanf("%d",& minutoInicio);
	
	if(minutoInicio < 0 || minutoInicio > 59) 
	{
		printf("\n Insira os minutos entre 0 e 59. \n\n");
		system("pause");
		system("cls");
		goto PEDIDOMINUTOINICIO; // Caso o numero inserido para os minutos iniciais for menor que 0 ou maior que 59 ele retorna, pedindo um novo valor
	}
	
	PEDIDOHORAFINAL:
	printf("\n HORARIO FINAL(HH): ");
	scanf("%d",& horaFinal);
	
	if(horaFinal < 0 || horaFinal > 23) 
	{
		printf("\n Insira uma hora entre 0 e 23. \n\n");
		system("pause");
		system("cls");
		goto PEDIDOHORAFINAL; // Caso o numero inserido para o horario inicial for menor que 0 ou maior que 23 ele retorna, pedindo um novo valor
	}
	
	PEDIDOMINUTOFINAL:
	printf("\n MINUTOS FINAIS(MM): ");
	scanf("%d",& minutoFinal);
	
	if(minutoFinal < 0 || minutoFinal > 59) 
	{
		printf("\n Insira os minutos entre 0 e 59. \n\n");
		system("pause");
		system("cls");
		goto PEDIDOMINUTOFINAL; // Caso o numero inserido para os minutos iniciais for menor que 0 ou maior que 59 ele retorna, pedindo um novo valor
	}
	
	if((horaFinal == horaInicio) && (minutoFinal == minutoInicio)){
	printf("\n O jogo teve 24h de duracao \n\n");
	system("pause");
	system("cls");
	goto INICIO;}
		
	//Acrescimo de 24 horas se o final passou da 0h00 (outro dia)
	if(horaFinal < horaInicio) horaFinal += 24;
	
	//Acrescimo de 23 horas e 60 minutos para impedir problema de inicio 0h02 e final 0h01
	if(horaFinal == horaInicio && minutoFinal < minutoInicio) minutoFinal += 1440;
	
	//Convers�o de horas para minutos com soma de minutos
	int horasInicio_Minutos = (horaInicio * 60) + minutoInicio;
	int horasFinal_Minutos = (horaFinal * 60) + minutoFinal;
	
	//Dura��o entre inicio e final em minutos
	int duracaoMinutos = horasFinal_Minutos-horasInicio_Minutos;
	
	//Calculo Final para Duracao
	int DuracaoH = duracaoMinutos/60;
	int DuracaoM = duracaoMinutos%60; // ou pode usar duracaoMinutos-(60*DuracaoH);;
	
	
	printf("\n O jogo teve %dh%.2dm de duracao", DuracaoH, DuracaoM);
	
	
	printf("\n\n Deseja fazer outra operacao?");
	op=getche(); op=toupper(op); } while(op != 'N');	
}
